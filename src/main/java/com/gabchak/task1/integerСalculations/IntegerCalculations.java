package com.gabchak.task1.integerСalculations;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class IntegerCalculations {

    public static int getMax(int a, int b, int c) {
        CountThreeInt max = (a1, b1, c1) -> Stream.of(a1, b1, c1).max(Integer::compareTo).get();
        return max.count(a, b, c);
    }

    public static int getAverage(int a, int b, int c) {
        CountThreeInt average = ((a1, b1, c1) -> (int) IntStream.of(a1, b1, c1).average().getAsDouble());
        return average.count(a, b, c);
    }

}
