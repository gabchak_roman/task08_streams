package com.gabchak.task1.integerСalculations;

@FunctionalInterface
public interface CountThreeInt {
    int count(int a, int b, int c);
}
