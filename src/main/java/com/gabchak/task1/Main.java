package com.gabchak.task1;

import com.gabchak.task1.integerСalculations.IntegerCalculations;

public class Main {
    public static void main(String[] args) {

        int a = 1;
        int b = 2;
        int c = 3;

        System.out.println("Max number: " + IntegerCalculations.getMax(a, b, c));
        System.out.println("Average number: " + IntegerCalculations.getAverage(a, b, c));

    }
}
