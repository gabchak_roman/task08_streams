package com.gabchak.task4.controller;

@FunctionalInterface
public interface Print {
    void print();
}