package com.gabchak.task4.controller;

import com.gabchak.task4.model.Model;
import com.gabchak.task4.view.View;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ControllerImpl implements Controller {

    private final int menuLineLength = 28;
    private Scanner input = new Scanner(System.in);
    private Model model;
    private View view;
    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;

    public ControllerImpl(Model modelIn, View viewIn) {
        this.model = modelIn;
        this.view = viewIn;
    }

    @Override
    public void start() {
        view.printMessageGreen(
                "----------------  Write a few lines to start the program ---------------");
        model.addText(input);
        menu();
        print();
    }

    private void menu() {
        menu = new LinkedHashMap<>();

        menu.put("1", "|        1  -  Show number of unique words             |");
        menu.put("2", "|        2  -  Show sorted list of unique words        |");
        menu.put("3", "|        3  -  Show words and occurrence number        |");
        menu.put("4", "|        4  -  Show each symbol occurrence number      |");
        menu.put("Q", "|        Q  -  Exit                                    |");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        view.printMessageGreen("--".repeat(menuLineLength));
        view.printMessage("Number of unique words: "
                + model.getNumberOfUniqueWords());
        view.printMessageGreen("--".repeat(menuLineLength));
    }

    private void pressButton2() {
        view.printMessageGreen("--".repeat(menuLineLength));
        view.printMessage("Sorted list of unique words: ");
        view.printMessageGreen("--".repeat(menuLineLength));
        view.printList(model.getSortedListOfUniqueWords());
    }

    private void pressButton3() {
        view.printMessageGreen("--".repeat(menuLineLength));
        view.printMessage("Occurrence word number and word: ");
        view.printMessageGreen("--".repeat(menuLineLength));
        view.printMap(model.getWordsAndOccurrenceNumber());
    }

    private void pressButton4() {
        view.printMessageGreen("--".repeat(menuLineLength));
        view.printMessage("Occurrence symbol number and symbol: ");
        view.printMessageGreen("--".repeat(menuLineLength));
        view.printMap(model.getSymbolAndOccurrenceNumber());
    }

    private void print() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessageGreen("▬▬".repeat(menuLineLength));
            view.printMessageGreen("Please, select menu point.  ");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        view.printMessageGreen("►◄".repeat(menuLineLength));
        view.printMessageGreen("                         MENU:");
        view.printMessageGreen("►◄".repeat(menuLineLength));
        menu.values().forEach(
                message -> view.printMessageGreen(message));
    }
}