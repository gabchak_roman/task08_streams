package com.gabchak.task4.view;

import java.util.List;
import java.util.Map;

public class ViewImpl implements View {

    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";

    @Override
    public final void printMessageGreen(final String message) {
        System.out.println(ANSI_GREEN + message + ANSI_RESET);
    }

    @Override
    public final void printMessage(final String message) {
        System.out.println("               " + message + "         ");
    }

    @Override
    public void printList(List<String> strings) {
        strings.forEach(this::printMessage);
    }

    @Override
    public void printMap(Map<String, Long> map) {
        for (Map.Entry<String, Long> values : map.entrySet()) {
            printMessage("Occurs: " + values.getValue()
                    + " -> " + values.getKey());
        }
    }
}
