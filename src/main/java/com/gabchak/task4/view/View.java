package com.gabchak.task4.view;

import java.util.List;
import java.util.Map;

public interface View {

    void printMessageGreen(String message);

    void printMessage(String message);

    void printList(List<String> strings);

    void printMap(Map<String, Long> map);

}
