package com.gabchak.task4;

import com.gabchak.task4.controller.Controller;
import com.gabchak.task4.controller.ControllerImpl;
import com.gabchak.task4.model.Model;
import com.gabchak.task4.model.ModelImp;
import com.gabchak.task4.view.View;
import com.gabchak.task4.view.ViewImpl;

public class Application {
    public static void main(String[] args) {

        View view = new ViewImpl();
        Model model = new ModelImp();
        Controller controller = new ControllerImpl(model, view);
        controller.start();

    }
}
