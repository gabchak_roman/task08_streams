package com.gabchak.task4.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ModelImp implements Model {

    private String text;
    private String[] words;

    @Override
    public void addText(Scanner input) {
        StringBuilder inputData = new StringBuilder();
        while (input.hasNextLine()) {
            String temp = input.nextLine();
            if (temp.isEmpty()) {
                break;
            } else {
                inputData.append(temp).append(" ");
            }
        }
        text = inputData.toString();
        if (text.replaceAll("\\s+", "").isEmpty()) {
            System.exit(0);
        }
        words = text.toLowerCase().
                replaceAll("[.,?–\"':;+(){}<>-].*?", " ")
                .split("\\s+");
    }

    @Override
    public int getNumberOfUniqueWords() {
        return Arrays.stream(words)
                .collect(Collectors.toSet())
                .size();
    }

    @Override
    public List<String> getSortedListOfUniqueWords() {
        return Arrays.stream(words)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Long> getWordsAndOccurrenceNumber() {
        return Arrays.stream(words)
                .collect(Collectors.groupingBy(
                        Function.identity(), Collectors.counting()));

    }

    @Override
    public Map<String, Long> getSymbolAndOccurrenceNumber() {
        String lower = text.chars()
                .filter(Character::isLowerCase)
                .mapToObj(c -> Character.toString((char) c))
                .collect(Collectors.joining());
        return Arrays.stream(lower.split(""))
                .collect(Collectors.groupingBy(
                        c -> c, Collectors.counting()));
    }
}