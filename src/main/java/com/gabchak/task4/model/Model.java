package com.gabchak.task4.model;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public interface Model {

    void addText(Scanner input);

    int getNumberOfUniqueWords();

    List<String> getSortedListOfUniqueWords();

    Map<String, Long> getWordsAndOccurrenceNumber();

    Map<String, Long> getSymbolAndOccurrenceNumber();
}
