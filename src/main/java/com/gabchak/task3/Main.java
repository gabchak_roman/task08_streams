package com.gabchak.task3;

import com.gabchak.task3.utils.PrintInfo;
import com.gabchak.task3.utils.RandomGenerator;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        int limit = 10;
        int lower = 0;
        int higher = 10;

        int[] array = RandomGenerator.intArray1(limit, lower, higher);
        PrintInfo.printIntArray(array);

        List<Integer> list = RandomGenerator.IntegerList(limit, lower, higher);
        PrintInfo.printIntegerList(list);

    }
}
