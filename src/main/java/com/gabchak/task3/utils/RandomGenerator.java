package com.gabchak.task3.utils;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class RandomGenerator {

    private static Random random = new Random();

    public static int[] intArray1(int limit, int lower, int higher) {
        return random
                .ints(limit, lower, higher)
                .toArray();
    }

    public static int[] intArray2(int limit, int lower, int higher) {
        return IntStream.generate(() -> new Random()
                .nextInt(lower) + higher)
                .limit(limit).toArray();
    }

    public static Integer[] IntegerArray(int limit, int lower, int higher) {
        return random.
                ints(limit, lower, higher)
                .boxed()
                .toArray(Integer[]::new);
    }

    public static List<Integer> IntegerList(int limit, int lower, int higher) {
        return random.
                ints(limit, lower, higher).
                boxed().
                collect(toList());
    }

}
