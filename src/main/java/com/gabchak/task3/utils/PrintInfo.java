package com.gabchak.task3.utils;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;

public class PrintInfo {

    public static void printIntArray(int[] array) {
        IntSummaryStatistics arrayStatistic = Arrays.stream(array).summaryStatistics();
        System.out.println("Array     -> : " + Arrays.toString(array));
        System.out.println("Average   -> : " + (int) arrayStatistic.getAverage());
        System.out.println("Min       -> : " + arrayStatistic.getMin());
        System.out.println("Max       -> : " + arrayStatistic.getMax());
        System.out.println("Sum       -> : " + arrayStatistic.getSum());

        int sumStreamArr = Arrays.stream(array).sum();
        int sumReduceArr = Arrays.stream(array).reduce(Integer::sum).getAsInt();
        int sumOfNumBiggerThenAverageArr = (int) Arrays.stream(array)
                .filter(value -> value > arrayStatistic.getAverage())
                .count();

        System.out.println("SumStream -> : " + sumStreamArr);
        System.out.println("SumReduce -> : " + sumReduceArr);
        System.out.println("Sum of numbers bigger than average -> : " + sumOfNumBiggerThenAverageArr + "\n");
    }

    public static void printIntegerList(List<Integer> list) {
        IntSummaryStatistics listStatistic = list.stream().
                mapToInt(Integer::intValue)
                .summaryStatistics();

        System.out.println("Int list  -> : " + list);
        System.out.println("Average   -> : " + (int) listStatistic.getAverage());
        System.out.println("Min       -> : " + listStatistic.getMin());
        System.out.println("Max       -> : " + listStatistic.getMax());
        System.out.println("Sum       -> : " + listStatistic.getSum());

        int sumStream = list.stream().mapToInt(Integer::intValue).sum();
        int sumReduce = list.stream().reduce(Integer::sum).orElse(0);
        int sumOfNumBiggerThenAverage = (int) list.stream()
                .mapToInt(Integer::intValue)
                .filter(value -> value > listStatistic.getAverage())
                .count();

        System.out.println("SumStream -> : " + sumStream);
        System.out.println("SumReduce -> : " + sumReduce);
        System.out.println("Sum of numbers bigger than average -> : " + sumOfNumBiggerThenAverage + "\n");
    }

}
