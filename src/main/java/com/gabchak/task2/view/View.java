package com.gabchak.task2.view;

public interface View {

    void print(String message);

    void printLine();
}
