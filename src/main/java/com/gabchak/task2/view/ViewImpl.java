package com.gabchak.task2.view;

public class ViewImpl implements View {
    @Override
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public void printLine() {
        System.out.println("| --------------------------------------------------------------------------- |");
    }
}
