package com.gabchak.task2.model.commands;

import com.gabchak.task2.model.reciver.ChangeStringColor;

public class PrintBlueMessage implements Command {

    ChangeStringColor changeStringColor = new ChangeStringColor();

    @Override
    public void execute(String string) {
        changeStringColor.printBlueString(string);
    }
}
