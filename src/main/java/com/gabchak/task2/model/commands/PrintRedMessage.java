package com.gabchak.task2.model.commands;

import com.gabchak.task2.model.reciver.ChangeStringColor;

public class PrintRedMessage implements Command {
    ChangeStringColor colorMessage = new ChangeStringColor();

    @Override
    public void execute(String argument) {
        colorMessage.printRedString(argument);
    }
}
