package com.gabchak.task2.model.commands;

public interface Command {
    void execute(String argument);
}
