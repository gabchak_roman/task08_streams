package com.gabchak.task2.model.invoker;

import com.gabchak.task2.model.Model;
import com.gabchak.task2.model.commands.*;

import java.util.HashMap;
import java.util.Map;

public class Commands implements Model {

    private Map<String, Command> commands = new HashMap<>();

    public Commands() {
        commands.put("b", argument -> new PrintBlueMessage().execute(argument));
        commands.put("g", this::printGreen);
        commands.put("r", new Command() {
            @Override
            public void execute(String argument) {
                new PrintRedMessage().execute(argument);
            }
        });
        commands.put("y", new PrintYellowMessage());
    }

    private void printGreen(String string) {
        new PrintGreenMessage().execute(string);
    }

    public Map<String, Command> getCommands() {
        return commands;
    }
}