package com.gabchak.task2.model;

import com.gabchak.task2.model.commands.Command;

import java.util.Map;

public interface Model {
    Map<String, Command> getCommands();
}
