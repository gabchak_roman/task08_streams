package com.gabchak.task2.model.reciver;

public class ChangeStringColor {

    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";

    public ChangeStringColor() {
    }

    public void printBlueString(String string) {
        System.out.println(ANSI_BLUE + string + ANSI_RESET);
    }

    public void printGreenString(String string) {
        System.out.println(ANSI_GREEN + string + ANSI_RESET);
    }

    public void printRedString(String string) {
        System.out.println(ANSI_RED + string + ANSI_RESET);
    }

    public void printYellowString(String string) {
        System.out.println(ANSI_YELLOW + string + ANSI_RESET);
    }

}
