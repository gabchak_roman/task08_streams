package com.gabchak.task2.controller;

import com.gabchak.task2.model.Model;
import com.gabchak.task2.view.View;

import java.util.Scanner;

public class ControllerImpl implements Controller {

    private Scanner input = new Scanner(System.in);
    private Model model;
    private View view;

    public ControllerImpl(Model modelIn, View viewIn) {
        this.model = modelIn;
        this.view = viewIn;
    }

    @Override
    public void start() {
        view.print("| ------------------- Welcome to the text color changer --------------------- |");
        menuStart();
    }

    private void menuStart() {
        printMenu();
        String command = input.nextLine().toLowerCase();
        if (model.getCommands().containsKey(command)) {
            view.print("| ------------------------- Please type some text -------------------------- |");
            model.getCommands().get(command).execute(getInputText());
            menuStart();
        } else if (command.equalsIgnoreCase("q")) {
            System.exit(0);
        } else {
            view.print("  ---------------------------- No such command! -----------------------------\n");
            menuStart();
        }
    }

    private String getInputText() {
        StringBuilder text = new StringBuilder();
        String line;
        do {
            line = input.nextLine();
            text.append(line).append("\n");
        } while (!line.isEmpty());
        return text.toString();
    }


    private void printMenu() {
        view.printLine();
        view.print("|        To select text color, enter one of the following commands:           |\n"
                + "|   'b' - blue                                                                |\n"
                + "|   'g' - green                                                               |\n"
                + "|   'r' - red                                                                 |\n"
                + "|   'y' - yellow                                                              |\n"
                + "|   'q' - Exit                                                                |");
        view.printLine();
        view.print("| Please enter a command: ");
    }
}