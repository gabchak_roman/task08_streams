package com.gabchak.task2;

import com.gabchak.task2.controller.Controller;
import com.gabchak.task2.controller.ControllerImpl;
import com.gabchak.task2.model.Model;
import com.gabchak.task2.model.invoker.Commands;
import com.gabchak.task2.view.View;
import com.gabchak.task2.view.ViewImpl;

public class Application {
    public static void main(String[] args) {
        View view = new ViewImpl();
        Model model = new Commands();
        Controller controller = new ControllerImpl(model, view);
        controller.start();
    }
}
